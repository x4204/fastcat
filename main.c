#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <sys/syscall.h>

int has_splice()
{
  long int err;

  err = syscall(SYS_splice, STDIN_FILENO, NULL, STDERR_FILENO, NULL, 0, 0);
  if (err < 0 && errno == ENOSYS) {
    return 0;
  }
  return 1;
}

int cat_splice(int fd)
{
  ssize_t bytes_moved;
  int pipefd[2];
  size_t buffer_size;
  int err;
  size_t buffer_page_count;
  size_t buffer_page_size;

  err = pipe(pipefd);
  if (err < 0) {
    return err;
  }
  buffer_page_count = (size_t)sysconf(_PC_PIPE_BUF);
  buffer_page_size = (size_t)fpathconf(pipefd[1], _PC_PIPE_BUF);
  buffer_size = buffer_page_count * buffer_page_size;
  while (1) {
    bytes_moved = splice(fd, NULL, pipefd[1], NULL, buffer_size, SPLICE_F_MOVE);
    if (bytes_moved <= 0) {
      break;
    }
    splice(pipefd[0], NULL, STDOUT_FILENO, NULL, bytes_moved, SPLICE_F_MOVE);
  }
  close(pipefd[1]);
  close(pipefd[0]);
  return 0;
}

int cat_default(int fd)
{
  char *buffer;
  size_t buffer_size;
  ssize_t bytes_read;
  ssize_t bytes_written;
  ssize_t buffer_index;
  int err;

  err = posix_fadvise(fd, 0, 0, POSIX_FADV_SEQUENTIAL);
  if (err != 0) {
    return err;
  }
  buffer_size = (size_t)sysconf(_SC_PAGESIZE) * 4;
  buffer = (char *)malloc(sizeof(char) * buffer_size);
  if (buffer == NULL) {
    return -1;
  }
  while (1) {
    bytes_read = read(fd, buffer, buffer_size);
    if (bytes_read <= 0) {
      break;
    }
    buffer_index = 0;
    while (buffer_index < bytes_read) {
      bytes_written = write(STDOUT_FILENO, &(buffer[buffer_index]), bytes_read);
      buffer_index += bytes_written;
    }
  }
  free(buffer);
  return 0;
}

int cat(const char *file_name)
{
  int fd;
  int err;

  fd = open(file_name, O_RDONLY);
  if (fd < 0) {
    return -1;
  }
  if (has_splice()) {
    err = cat_splice(fd);
  } else {
    err = cat_default(fd);
  }
  close(fd);
  return err;
}

int main(int argc, char **argv)
{
  int err;

  if (argc == 1) {
    printf("usage: %s <...files>\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  for (int i = 1; i < argc; i++) {
    err = cat(argv[i]);
    if (err < 0) {
      printf("[%d|%d]: %s\n", err, errno, strerror(errno));
    }
  }
  return 0;
}
